import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare const FB :any;

@Injectable({
  providedIn: 'root'
})

export class CommonServicesService {

  baseUrl: any = 'http://ec2-18-188-196-204.us-east-2.compute.amazonaws.com:3000'

  constructor(private http: HttpClient) {
    FB.init({
      appId      : '375437676211645',
      status     : false, // the SDK will attempt to get info about the current user immediately after init
      cookie     : false,  // enable cookies to allow the server to access
      // the session
      xfbml      : false,  // With xfbml set to true, the SDK will parse your page's DOM to find and initialize any social plugins that have been added using XFBML
      version    : 'v2.8' // use graph api version 2.5
    });
   }

  loginService (data) {
    return this.http.get(`${this.baseUrl}/admin/adminLogin`, data);
  }

  loginWithPromise (data) {
    return new Promise ((resolve, reject) => {
      this.http.get(`${this.baseUrl}/admin/adminLogin`, data).toPromise().then(response =>{
        resolve(response)
      }).catch(error=> {
        reject(error)
        console.log(error)
      })
    })
    debugger;
  }

  fbLogin() {
    return new Promise((resolve, reject) => {
      FB.login(result => {
        if (result.authResponse) {
          return this.http.post(`http://localhost:4200/api/v1/auth/facebook`, {access_token: result.authResponse.accessToken})
              .toPromise()
              .then(response => {
                var token = response['headers'].get('x-auth-token');
                if (token) {
                  localStorage.setItem('id_token', token);
                }
                resolve(response);
              })
              .catch(() => reject());
        } else {
          reject();
        }
      }, {scope: 'public_profile,email'})
    });
  }
}
