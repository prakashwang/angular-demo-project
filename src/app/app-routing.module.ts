import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { CommonHeaderComponent } from './components/common-header/common-header.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { LoginGuard } from './guards/login.guard';
import { AuthCheckGuard } from './guards/auth-check.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  { path: '', component: CommonHeaderComponent, children: [
    { path: 'dashboard', component: HomePageComponent, canActivate:[LoginGuard], canDeactivate: [AuthCheckGuard], data: { 'name': localStorage.getItem('name') }
    }
  ]},
  { path: '**', component: ErrorPageComponent }
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { 
 }
