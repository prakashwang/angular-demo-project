import { CanDeactivate } from "@angular/router";
import { LoginPageComponent } from "../components/login-page/login-page.component";

export class SaveFormsGuard implements CanDeactivate<LoginPageComponent> {
    constructor() {}

    canDeactivate() {
        return true;
    }
}
