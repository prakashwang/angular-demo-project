import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonServicesService } from '../../services/common-services.service';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ToastrService } from 'ngx-toastr';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  login_form: FormGroup;
  login_data: any = {};

  constructor(private common_service: CommonServicesService, private socialAuthService: AuthService, private toastrService: ToastrService) {
    this.login_form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
  }
  
  login_subscribe() {
    // this.router.navigate('/dashboard')
    console.log(this.login_data)
    let req_body = {
      email: this.login_data.email,
      password: this.login_data.password
    }
    this.common_service.loginService(req_body).subscribe((response) => {
      console.log(response)
      this.toastrService.success('Hello world!', 'Toastr fun!');
    }, (reject) => {
      console.log(reject)
      this.toastrService.success('Hello world!', 'Toastr fun!');
    })
  }

  login_promise() {
    console.log(this.login_data)
    let req_body = {
      email: this.login_data.email,
      password: this.login_data.password
    }
    this.common_service.loginWithPromise(req_body).then(res =>{

    }).catch(err => {

    })
  }

  fbLogin() {
    // this.common_service.fbLogin().then(res=>{
    //   console.log(res)
    // }).catch(err=> {
    //   console.log(err)
    // })
    let socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("Facebook sign in data : " , userData);
        // Now sign-in with userData
        // ...  
      }
    )
  }

  googleLogin() {
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("Google sign in data : " , userData);
        // Now sign-in with userData
        // ...  
      }
    )
  }

}
