import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  nextId: number = 0;
  constructor() { }

  ngOnInit() {
    this.logIt(`this is updated message ${this.nextId++}`, `${++this.nextId}`)
  }
  logIt(msg, nextId) {
    console.log(`#${nextId++} ${msg}`);
  }

}
