import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { LoginPageComponent } from '../components/login-page/login-page.component';

@Injectable({
  providedIn: 'root'
})
export class AuthCheckGuard implements CanDeactivate<LoginPageComponent> {
  canDeactivate(){
    console.log('true')
    return true;
  }
}
