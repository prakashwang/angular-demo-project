const express = require('express');
const app = express();
const port = 3000;
const path = require('path');

app.listen(port);

app.use(express.static('dist/demo'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/');
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/dist/demo/index.html');
});

app.use(express.static(path.join(__dirname, 'dist/demo')));


app.all('/*', function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

console.log("app is listing on port-->>",port);